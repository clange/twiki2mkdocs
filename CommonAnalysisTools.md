# Common Analysis Tools

<span class="twiki-macro IMAGE" align="right" size="500">Cat2.png</span>

The Common Analysis Tools (CAT) group is charged with two main tasks:

1.  Take ownership of the development, maintenance and documentation of
    analysis tools that are considered of common interest to the
    collaboration. 2. Provide a forum to discuss developments of new
    analysis tools, and offer guidance such that some of these tools can
    become centrally maintained and handled as in point 1.

**Current conveners:** Piergiulio Lenzi (Firenze) and Clemens Lange
(PSI)

**Contact:** <cms-phys-conveners-CAT@cern.ch>

**Announcements and discussions:** [CMSTalk
forum](https://cms-talk.web.cern.ch/c/physics/cat/274), email gateway
<cmstalk+cat@dovecotmta.cern.ch>

**Meetings**:

- Time: Every two weeks on Wednesdays (odd weeks), 14:00-16:00 CERN time
- Location: online only
- Indico meeting category:
  [CMS/Physics/CommonAnalysisTools](https://indico.cern.ch/category/15986/)

<span class="twiki-macro N"></span> **Documentation page**
<https://cms-analysis.docs.cern.ch/>

<span class="twiki-macro TOC"></span>

## Mandate

The CAT group will handle analysis tools not covered by groups in other
coordination areas. In particular, the following items are explicitly
considered part of the [mandate of the
group](https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=14441):

- Post-NanoAOD processing of events to compute derived analysis level
  quantities.
- Tools needed for final steps of data reduction (e.g. histogramming).
- Aggregators of meta-data needed to book keep sample normalization and
  weights.
- Tools for handling intermediate caching of processed data (e.g.
  intermediate ntuples).
- Plotting tools (beyond the primitives offered by the ROOT/python
  tool-kits).
- Tools to create signal/background models for later statistical
  interpretation (e.g. datacards).
- Tools to provide statistical interpretations (e.g. combine).
- Top level analysis descriptions, orchestration of the analysis
  components and analysis preservation capabilities.
- Interfaces for interactive execution or for continuous integration
  pipelines.

## Subgroups

### Data Processing Tools

**Current conveners:** Andrzej Novak (Aachen) and Tommaso Tedeschi
(Perugia)

This subgroup has the responsibility for the support, management, and
development of tools running directly on the CMS data tiers. In
particular:

- Offers support and a forum for discussion of moving physics analysis
  code to RDataFrame and/or uproot/AwkwardArrays/Coffea; analysis
  frameworks using event loops will also be supported.
- Fosters the development of common code for the propagation of
  corrections and systematic uncertainties on top of the CMS NanoAOD
  data tier leveraging correctionlib, also supporting, on a best-effort
  basis, NanoAOD-tools for what concerns its event data model.
- In collaboration with CrossPOG, promotes the development of tools for
  the customization of NanoAOD event content (e.g. for the integration
  of special reconstruction algorithms).
- In collaboration with PPD, promotes the development of user-friendly
  dataset management tools (leveraging dasgoclient, Grasp, XSDB, etc).
- In collaboration with O&C, represents the software needs of physics
  analyses in the context of the development of analysis facilities and
  other innovative computing infrastructures.
- Provide pointers to and actual documentation on all points mentioned
  above.

### Workflow Orchestration and Analysis Preservation

**Current conveners:** Marcel Rieger (Hamburg) and Sihyun Jeon (Seoul)

This subgroup has the responsibility for the support, management, and
development of tools for the orchestration of physics analysis
workflows, promoting tools that ease the long-term reproducibility of
analyses. In particular:

- Offers support and a forum for discussion of tools that bridge the gap
  between the output of Data Processing Tools and the input to
  Statistical Interpretation Tools and that connect these steps as part
  of the analysis workflow. This in particular includes the application
  of various correction and scale factors, the book-keeping of various
  physics processes and samples, and the evaluation of systematic
  uncertainties.
- Supports the exploration and possible adoption of good practices in
  analysis description, towards a standardization of the analysis
  workflow for the most common patterns.
- Provides and maintains examples of analysis-specific physics object
  calibration and background estimation techniques.
- Provides complete physics analysis examples of different complexity
  and keeps them up-to-date with the latest recommendations
- Manages an analysis code repository and accompanying templates for
  continuous integration and software testing.
- Supports the standardization of analysis metadata and HEPData records.
- Supports and documents continuous integration/software testing
  practices.
- Supports and documents plotting tools.
- In collaboration with DPOA, integrates with CERN services such as
  REANA and analysis preservation portal.

### Statistical Interpretation Tools

**Current conveners:** Aliya Nigamova (Hamburg) and Kyle Cormier
(Zurich)

This subgroup has the responsibility for the support, management and
development of statistical interpretation tools. In particular:

- Provides a forum for technical discussion of Combine issues,
  leveraging the expertise already available in the core Combine
  developer group.
- Supports maintenance, development and documentation of Combine and
  Combine Harvester and any other statistical tools widely used in CMS.
- Coordinates the existing Combine/statistics contacts within the
  Physics Analysis Groups for technical validation of datacards
- Maintains a central CMS datacard repository.
- Organizes training events for Combine.
- Explores other statistical inference tools and their interoperability
  with Combine, including alternatives to RooFit such as pyHF and zfit.
- Provides actual/pointers to (technical) documentation on the relevant
  tools.

## Interaction with POGs

Via contact persons identified by the POG (possibly among existing L3s).
Contacts have the responsibility of communicating changes in
recommendations, and implement those changes in the analysis examples
provided.

## Interactions with PAGs

Via dedicated contact persons identified by the PAG. Contact persons
should attend the meetings, report on analysis development in the PAG,
establish a link between the CAT and the people developing analysis
tools in the PAGs.

## Interaction with O&C

Via a dedicated liaison.

## Interaction with the HEP Statistics Serialization Standard ([HS3](https://github.com/hep-statistics-serialization-standard/hep-statistics-serialization-standard) Initiative)

Kirill Skovpen.

-- Main.ClemensLange - 2022-09-28
