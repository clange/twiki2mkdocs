# TWiki to MkDocs

## Obtain SSO cookie

e.g. on lxplus:

```shell
auth-get-sso-cookie -u 'https://twiki.cern.ch/twiki/bin/view/CMS/CommonAnalysisTools' -o cookies.txt
```

## Download page and convert

**Mind**: `pandoc` is not available on lxplus.

```shell
curl -b cookies.txt -s --show-error 'https://twiki.cern.ch/twiki/bin/view/CMS/CommonAnalysisTools?raw=text' | iconv -f windows-1252 > CommonAnalysisTools.twiki
pandoc -f twiki -t gfm CommonAnalysisTools.twiki > CommonAnalysisTools.md
```
